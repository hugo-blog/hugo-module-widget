/*global window */
/*jshint bitwise:false */

/**
 * API entry
 * @public
 * @param {function(Object)|Date|number} start the starting date
 * @param {function(Object)|Date|number} end the ending date
 * @param {number} units the units to populate
 * @return {Object|number}
 */
var module, countdown = (

/**
 * @param {Object} module CommonJS Module
 */
function(module) {
  /*jshint smarttabs:true */
  'use strict';

  function getTimeRemaining(end) {
    const t = end - Date.parse(new Date());
    var v = t/1000;
    var s = Math.floor(v % 60);
    v /= 60;
    var m = Math.floor(v % 60);
    v /= 60;
    var h = Math.floor(v % 24);
    v /= 24;
    var d = Math.floor(v);
    return {
      'total': t,
      'days': d,
      'hours': h,
      'minutes': m,
      'seconds': s
    };
  }

  function initializeClock(id, endtime) {
    var clock = document.getElementById(id);
    var end = Date.parse(endtime);
    var daysSpan = clock.querySelector('.days');
    var hoursSpan = clock.querySelector('.hours');
    var minutesSpan = clock.querySelector('.minutes');
    var secondsSpan = clock.querySelector('.seconds');
  
    function updateClock() {
      var t = getTimeRemaining(end);
  
      daysSpan.innerHTML = t.days;
      hoursSpan.innerHTML = ('0' + t.hours).slice(-2);
      minutesSpan.innerHTML = ('0' + t.minutes).slice(-2);
      if (!!secondsSpan) secondsSpan.innerHTML = ('0' + t.seconds).slice(-2);
  
      if (t.total <= 0) {
        clearInterval(timeinterval);
      }
    }
  
    updateClock();
    var timeinterval = setInterval(updateClock, 1000);
  }

  if (module && module.exports) {
    module.exports = countdown;
  } else if (typeof window.define === 'function' && typeof window.define.amd !== 'undefined') {
    window.define('countdown', [], function() {
      return countdown;
    });
  }
  return initializeClock;
})(module);
